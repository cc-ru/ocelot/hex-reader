import java.awt.Color
import java.awt.image.{BufferedImage, RenderedImage}
import java.io.{File, FileOutputStream}

import javax.imageio.ImageIO

import scala.io.Source

object HexReader extends App {
  def hex2int(hex: String): Int = Integer.parseInt(hex, 16)
  def hex2int(hex: Char): Int = {
    val code = hex.toInt
    if (code >= 48 && code <= 57) code - 48
    else if (code >= 65 && code <= 70) code - 55
    else code - 87
  }

  def convertToBin(inputFile: String, outputFile: String) = {
    val out = new FileOutputStream(outputFile)

    println("Conversion started...")

    Source.fromFile(inputFile)
      .getLines()
      .foreach(line => {
        if (line != null && line.nonEmpty) {
          val delimiter = line.indexOf(':')
          val code: Int = hex2int(line.take(delimiter))
          val hex: String = line.drop(delimiter + 1)
          // write char code
          out.write(code / 256)
          out.write(code % 256)
          // for single characters the matrix will consist of 16 bytes,
          // for double - from 32 bytes
          out.write(if (hex.length == 32) 16 else 32)
          // char data
          hex.grouped(2).foreach(chunk => {
            out.write(hex2int(chunk.head) * 16 + hex2int(chunk.tail))
          })
        }
      })

    out.close()

    println("Done.")
  }

  def convertToJS(inputFile: String, outputFile: String): Unit = {
    val out = new FileOutputStream(outputFile)

    println("Conversion started...")

    out.write("var FONT = {\n".getBytes("UTF-8"))

    var i = 0
    Source.fromFile(inputFile)
      .getLines()
      .foreach(line => {
        if (line != null && line.nonEmpty) {
          val delimiterIndex = line.indexOf(':')
          val code: Int = hex2int(line.take(delimiterIndex))
          out.write(s"    $code: $i,\n".getBytes("UTF-8"))
          i += 1
        }
      })

    out.write("}\n".getBytes("UTF-8"))

    out.close()

    println("Done.")
  }

  def toBinary(i: Int): String =
    String.format("%4s", i.toBinaryString).replace(' ', '0')

  def renderToPng(inputFile: String, outputFile: String, size: Int): Unit = {
    println("Render started...")
    println(s"Target file: $outputFile, texture size: $size.")

    val image = new BufferedImage(size, size, BufferedImage.TYPE_BYTE_BINARY)
    val graphics = image.createGraphics()
    graphics.setColor(Color.white)
    graphics.fillRect(0, 0, size, size)
    graphics.setColor(Color.black)

    val CharWidth = 16
    val CharHeight = 16
    val Width = size / CharWidth
    var index = 0
    Source.fromFile(inputFile)
      .getLines()
      .foreach(line => {
        if (line != null && line.nonEmpty) {
          val delimiter = line.indexOf(':')
          val hex: String = line.drop(delimiter + 1)
          // render this char
          val isWide = hex.length > 32
          var pixelIndex = 0
          hex.flatMap(pixel => toBinary(hex2int(pixel))).foreach(pixel => {
            val value = hex2int(pixel)
            if (value == 1) {
              graphics.drawRect(
                (index % Width) * CharWidth + pixelIndex % (if (isWide) CharWidth else CharWidth / 2),
                (index / Width) * CharHeight + pixelIndex / (if (isWide) CharWidth else CharWidth / 2),
                0, 0
              )
            }
            pixelIndex += 1
          })
          index += 1
        }
      })

    graphics.dispose()

    val out = new File(outputFile)
    ImageIO.write(image, "png", out)

    println("Done.")
  }

  renderToPng("font.hex", "font.png", 2048)
}
